let completed =  1

let numbers = {};

let resultadoFinal = {};

let centenas = {0:"", 100:"cento e ", 200:"duzentos e ",
300:"trezentos e ", 400:"quatrocentos e ", 500:"quinhentos e ",
600:"seiscentos e ", 700:"setecentos e ", 800:"oitocentos e ",
900:"novecentos e "}

let dezenas = {0:"", 20:"vinte e ", 30:"trinta e ", 40:"quarenta e ", 
50:"cinquenta e ",60:"sessenta e ", 70:"setenta e ", 80:"oitenta e ",
 90:"noventa e "}

let words = {0:"zero", 1:"um", 2:"dois", 3:"três", 4:"quatro",
5: "cinco", 6:"seis", 7: "sete", 8: "oito", 9: "nove",10:"dez", 11: "onze",
12: "doze", 13:"treze", 14:"catorze", 15:"quinze", 16:"dezesseis", 
17: "dezessete", 18:"dezoito", 19:"dezenove", 20:"vinte"}

let unidade = 1

function numbersToWords () {
    for (a=0; a <= 1000; a++) {
        numbers[a] = a
    }
    return numbers
}

function numbersToWordsPt2 () {
    for (numbers[b=0]; b <= 20; b++) {
        resultadoFinal[b] = words[b]

    }
    for (numbers[c=21]; c<= 29; c++) {
        resultadoFinal[c] = dezenas[20] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[30] = "trinta";

    for (numbers[d=31]; d<= 39; d++) {
        resultadoFinal[d] = dezenas[30] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[40] = "quarenta";

    for (numbers[e=41]; e<= 49; e++) {
        resultadoFinal[e] = dezenas[40] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[50] = "cinquenta";

    for (numbers[f=51]; f<= 59; f++) {
        resultadoFinal[f] = dezenas[50] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[60] = "sessenta";

    for (numbers[g=61]; g<= 69; g++) {
        resultadoFinal[g] = dezenas[60] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[70] = "setenta";

    for (numbers[h=71]; h<= 79; h++) {
        resultadoFinal[h] = dezenas[70] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[80] = "oitenta";

    for (numbers[i=81]; i<= 89; i++) {
        resultadoFinal[i] = dezenas[80] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[90] = "noventa";

    for (numbers[j=91]; j<= 99; j++) {
        resultadoFinal[j] = dezenas[90] + words[unidade]
        unidade = unidade + 1
    }

    unidade = 1
    resultadoFinal[100] = "cem";

    return resultadoFinal

}

function numbersToWordsPt3 () {
    for (numbers[k=101]; k<=199; k++) {
        resultadoFinal[k] = centenas[100] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[200] = "duzentos";

    for (numbers[l=201]; l<=299; l++) {
        resultadoFinal[l] = centenas[200] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[300] = "trezentos";


    for (numbers[m=301]; m<=399; m++) {
        resultadoFinal[m] = centenas[300] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[400] = "quatrocentos";

    for (numbers[n=401]; n<=499; n++) {
        resultadoFinal[n] = centenas[400] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[500] = "quinhentos";

    for (numbers[m=501]; m<=599; m++) {
        resultadoFinal[m] = centenas[500] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[600] = "seiscentos";

    for (numbers[o=601]; o<=699; o++) {
        resultadoFinal[o] = centenas[600] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[700] = "setecentos";

    for (numbers[p=701]; p<=799; p++) {
        resultadoFinal[p] = centenas[700] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[800] = "oitocentos";

    for (numbers[q=801]; q<=899; q++) {
        resultadoFinal[q] = centenas[800] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[900] = "novecentos";

    for (numbers[r=901]; r<=999; r++) {
        resultadoFinal[r] = centenas[900] + resultadoFinal[completed]
        completed = completed + 1
    }

    completed = 1
    resultadoFinal[1000] = "mil";

}
//------------------------------------------
console.log (numbersToWords())
console.log (numbersToWordsPt2())
console.log (numbersToWordsPt3())
//------------------------------------------

let numberTotalHTML = "";
let numberHTML = document.getElementById ("numeros")

for (z = 0; z <= 1000; z++) {
    numberTotalHTML += numbers[z] + "<br> "
}

numberHTML.innerHTML = numberTotalHTML


//-------------------------------------------

let wordTotalHTML = "";
let wordHTML = document.getElementById ("palavras")

for (z = 0; z <= 1000; z++) {
    wordTotalHTML += resultadoFinal[z] + "<br>"
}

wordHTML.innerHTML = wordTotalHTML